#!/usr/bin/env python3

def sumar(a, b):
    return a + b


def restar(a, b):
    return a - b


if __name__ == "__main__":
    suma_1_2 = sumar(1, 2)
    print("La suma de 1 y 2 es:", suma_1_2)

    suma_3_4 = sumar(3, 4)
    print("La suma de 3 y 4 es:", suma_3_4)

    resta_5_6 = restar(5, 6)
    print("La resta de 5 y 6 es:", resta_5_6)

    resta_7_8 = restar(7, 8)
    print("La resta de 7 y 8 es:", resta_7_8)